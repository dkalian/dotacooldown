package com.dota.dotacooldown.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.GridView;

import com.dota.dotacooldown.R;
import com.dota.dotacooldown.adapter.TimingSpellsAdapter;
import com.dota.dotacooldown.db.DBOperations;

public class AddSpellActivity extends ActionBarActivity {
	EditText searchSpellEditText;
	GridView spellsGrid;
	TimingSpellsAdapter spellsAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_spell);

		// init Grid
		spellsGrid = (GridView) findViewById(R.id.activity_add_spell_spells_grid);
		spellsAdapter = new TimingSpellsAdapter(this);
		spellsGrid.setAdapter(spellsAdapter);
		spellsGrid.setOnItemLongClickListener(itemLongClickListener);
		spellsGrid.setOnItemClickListener(spellsGridItemClickListener);
		spellsGrid.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);

		// init search field
		searchSpellEditText = (EditText) findViewById(R.id.activity_add_spell_search_spell_field);
		searchSpellEditText.addTextChangedListener(searchSpellEditTextWatcher);
	}

	OnItemLongClickListener itemLongClickListener = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos,
				long arg3) {
			actionMode = startSupportActionMode(mActionModeCallback);
			if (spellsGrid.isItemChecked(pos)) {
				spellsGrid.setItemChecked(pos, false);
			} else {
				spellsGrid.setItemChecked(pos, true);
			}
			if (spellsGrid.getCheckedItemCount() <= 0) {
				actionMode.finish();
			}
			return true;
		}
	};

	// Grid of spells item click listener
	OnItemClickListener spellsGridItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
				long arg3) {
			if (actionMode == null) {
				DBOperations.Cooldown.addSpell(AddSpellActivity.this,
						spellsAdapter.getItem(pos).getId());
				finish();
			} else {
				if (spellsGrid.isItemChecked(pos)) {
					spellsGrid.setItemChecked(pos, true);
				} else {
					spellsGrid.setItemChecked(pos, false);
				}

				if (spellsGrid.getCheckedItemCount() <= 0) {
					actionMode.finish();
					actionMode = null;
				}
			}
		}
	};

	ActionMode actionMode;
	ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.add_spells_action_menu, menu);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode arg0, MenuItem menuItem) {
			SparseBooleanArray checkedItems = spellsGrid
					.getCheckedItemPositions();
			for (int i = 0; i < checkedItems.size(); i++) {
				DBOperations.Cooldown.addSpell(AddSpellActivity.this,
						spellsAdapter.getItem(checkedItems.keyAt(i)).getId());
			}
			actionMode.finish();
			finish();
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode arg0) {
			actionMode = null;
			spellsGrid.clearChoices();
			spellsGrid.requestLayout();
		}

		@Override
		public boolean onPrepareActionMode(ActionMode arg0, Menu arg1) {
			return false;
		}
	};

	TextWatcher searchSpellEditTextWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			spellsAdapter.getFilter().filter(s);
			spellsAdapter.notifyDataSetChanged();
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}

	};

}
