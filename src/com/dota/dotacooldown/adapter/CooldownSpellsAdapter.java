package com.dota.dotacooldown.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dota.dotacooldown.R;
import com.dota.dotacooldown.db.DBOperations;
import com.dota.dotacooldown.model.Spell;
import com.dota.dotacooldown.view.ProgressButton;

public class CooldownSpellsAdapter extends BaseAdapter {

	private static final String TAG = "Cooldown Adapter Logger";

	ArrayList<Spell> cooldownSpells;
	Context context;

	public CooldownSpellsAdapter(Context context) {
		this.context = context;
		updateDataSet();
	}

	public void updateDataSet() {
		cooldownSpells = DBOperations.Cooldown.getAllSpells(context);
	}

	@Override
	public int getCount() {
		return cooldownSpells.size();
	}

	@Override
	public Spell getItem(int arg0) {
		return cooldownSpells.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int pos, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.item_spell_cooldown, null);
		}

		ImageView picture = (ImageView) view
				.findViewById(R.id.item_spell_cooldown_picture);
		TextView timerText = (TextView) view
				.findViewById(R.id.item_spell_cooldown_time);
		ProgressButton progressButton = (ProgressButton) view
				.findViewById(R.id.item_spell_cooldown_progress);
		picture.setImageResource(cooldownSpells.get(pos).getPictureId());
		TextView level = (TextView) view
				.findViewById(R.id.item_spell_cooldown_level);
		int cooldownInSeconds = 0;
		progressButton.setProgress(0);
		SharedPreferences defaultPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		int delayTime = (Integer.parseInt(defaultPreferences.getString("settings_key_delay_time", "3")));
		// set max value
		switch (cooldownSpells.get(pos).getCooldownLevel()) {
		case 1:
			progressButton.setMax(cooldownSpells.get(pos).getCooldown1() - delayTime);
			break;
		case 2:
			progressButton.setMax(cooldownSpells.get(pos).getCooldown2() - delayTime);
			break;
		case 3:
			progressButton.setMax(cooldownSpells.get(pos).getCooldown3() - delayTime);
			break;
		// FOR BKB!!!!
		case 4:
			// HARDCODED BKB 4 COOLDOWN!!!!!!!
			progressButton.setMax(65 - delayTime);
			break;
		// HARDCODED BKB 5 COOLDOWN!!!!!!!
		case 5:
			progressButton.setMax(60 - delayTime);
			break;
		// HARDCODED BKB 5 COOLDOWN!!!!!!!
		case 6:
			progressButton.setMax(55 - delayTime);
			break;

		}

		switch (cooldownSpells.get(pos).getCooldownLevel()) {
		case 1:
			cooldownInSeconds = (int) ((cooldownSpells.get(pos)
					.getTimerStartTime()
					+ (cooldownSpells.get(pos).getCooldown1() * 1000) - System
					.currentTimeMillis()) / 1000) - delayTime;
			break;
		case 2:
			cooldownInSeconds = (int) ((cooldownSpells.get(pos)
					.getTimerStartTime()
					+ (cooldownSpells.get(pos).getCooldown2() * 1000) - System
					.currentTimeMillis()) / 1000) - delayTime;
			break;
		case 3:
			cooldownInSeconds = (int) ((cooldownSpells.get(pos)
					.getTimerStartTime()
					+ (cooldownSpells.get(pos).getCooldown3() * 1000) - System
					.currentTimeMillis()) / 1000) - delayTime;
			break;
		// FOR BKB!!!
		// HARDCODED BKB 4 COOLDOWN!!!!!!!
		case 4:
			cooldownInSeconds = (int) ((cooldownSpells.get(pos)
					.getTimerStartTime() + (65 * 1000) - System
					.currentTimeMillis()) / 1000) - delayTime;
			break;
		// FOR BKB!!!
		// HARDCODED BKB 5 COOLDOWN!!!!!!!
		case 5:
			cooldownInSeconds = (int) ((cooldownSpells.get(pos)
					.getTimerStartTime() + (60 * 1000) - System
					.currentTimeMillis()) / 1000) - delayTime;
			break;

		// FOR BKB!!!
		// HARDCODED BKB 6 COOLDOWN!!!!!!!
		case 6:
			cooldownInSeconds = (int) ((cooldownSpells.get(pos)
					.getTimerStartTime() + (55 * 1000) - System
					.currentTimeMillis()) / 1000) - delayTime;
			break;

		}
		if (cooldownSpells.get(pos).getName().equalsIgnoreCase("bkb")) {
			level.setText((11 - cooldownSpells.get(pos).getCooldownLevel())
					+ " sec");
		} else {
			level.setText("level " + cooldownSpells.get(pos).getCooldownLevel());
		}
		
		if (cooldownSpells.get(pos).getName().equalsIgnoreCase("Refresher Orb")) {
			view.findViewById(R.id.item_spell_cooldown_level_bar).setVisibility(View.INVISIBLE);
		}
		if (cooldownInSeconds > 0) {
			timerText.setText(String.valueOf(cooldownInSeconds));
			progressButton.setProgress(cooldownInSeconds);
		} else {
			cooldownInSeconds = 0;
			timerText.setText("");
			progressButton.setProgress(0);
		}

		return view;
	}
}
