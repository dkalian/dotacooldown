package com.dota.dotacooldown.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dota.dotacooldown.R;

public class DAO extends SQLiteOpenHelper {

	public DAO(Context context) {
		super(context, "DOTA", null, 1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table " + DBConstant.DB_TABLE_SPELLS + " ("
				+ DBConstant.SPELL_ID + " integer primary key autoincrement,"
				+ DBConstant.SPELL_NAME + " text, " + DBConstant.SPELL_PICTURE
				+ " int, " + DBConstant.SPELL_COOLDOWN_1 + " int, "
				+ DBConstant.SPELL_COOLDOWN_2 + " int, "
				+ DBConstant.SPELL_COOLDOWN_3 + " int, "
				+ DBConstant.SPELL_HERO + " text);");
		initTable(db);
		
		db.execSQL("create table " + DBConstant.DB_TABLE_COOLDOWN + " ("
				+ DBConstant.TABLE_COOLDOWN_ID + " integer primary key autoincrement,"
				+ DBConstant.TABLE_COOLDOWN_ADD_TIME + " long,"
				+ DBConstant.TABLE_COOLDOWN_COOLDOWN_LVL+ " integer,"
				+ DBConstant.TABLE_COOLDOWN_SPELL_ID + " integer);");	
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	private void initTable(SQLiteDatabase db) {
		db.execSQL(getInsertSQL("abaddon", "borrowed time", R.drawable.borrowed_time, 60, 50, 40));
		db.execSQL(getInsertSQL("ancient apparation", "ice blast", R.drawable.ice_blast, 40, 40, 40));
		db.execSQL(getInsertSQL("anti mage", "mana void", R.drawable.mana_void, 70, 70, 70));
		db.execSQL(getInsertSQL("bane", "fiends grip", R.drawable.fiends_grip, 100, 100, 100));
		db.execSQL(getInsertSQL("batrider", "flaming lasso", R.drawable.flaming_lasso, 90, 75, 60));
		db.execSQL(getInsertSQL("bloodseeker", "rupture", R.drawable.rupture, 60, 60, 60));
		db.execSQL(getInsertSQL("brewmaster", "primal split", R.drawable.primal_split, 140, 120, 100));
		db.execSQL(getInsertSQL("centaur warrunner", "stampede", R.drawable.stampede, 90, 75, 60));
		db.execSQL(getInsertSQL("chaos knight", "phantasm", R.drawable.phantasm, 140, 140, 140));
		db.execSQL(getInsertSQL("chen", "hand of god", R.drawable.hand_of_god, 160, 140, 30));
		db.execSQL(getInsertSQL("clockwerk", "hookshot", R.drawable.hookshot, 70, 55, 40));
		db.execSQL(getInsertSQL("crystal maiden", "freezing field", R.drawable.freezing_field, 150, 120, 90));
		db.execSQL(getInsertSQL("dark seer", "wall of replica", R.drawable.wall_of_replica, 100, 100, 100));
		db.execSQL(getInsertSQL("death prophet", "exorcism", R.drawable.exorcism, 135, 135, 135));
		db.execSQL(getInsertSQL("disruptor", "glimpse", R.drawable.glimpse, 65, 50, 35));
		db.execSQL(getInsertSQL("disruptor", "static storm", R.drawable.static_storm, 85, 85, 85));
		db.execSQL(getInsertSQL("doom", "doom", R.drawable.doom, 100, 100, 100));
		db.execSQL(getInsertSQL("dragon knight", "elder dragon form", R.drawable.elder_dragon_form, 115, 115, 115));
		db.execSQL(getInsertSQL("earthshaker", "echo slam", R.drawable.echo_slam, 150, 130, 110));
		db.execSQL(getInsertSQL("elder titan", "earth splitter", R.drawable.earth_splitter, 100, 100, 100));
		db.execSQL(getInsertSQL("enigma", "black hole", R.drawable.black_hole, 200, 190, 180));
		db.execSQL(getInsertSQL("faceless void", "chronosphere", R.drawable.chronosphere, 120, 100, 80));
		db.execSQL(getInsertSQL("invoker", "chaos meteor", R.drawable.chaos_meteor, 55, 55, 55));
		db.execSQL(getInsertSQL("juggernaut", "omnislash", R.drawable.omnislash, 130, 120, 110));
		db.execSQL(getInsertSQL("legion commander", "duel", R.drawable.duel, 50, 50, 50));
		db.execSQL(getInsertSQL("lich", "chain frost", R.drawable.chain_frost, 145, 115, 60));
		db.execSQL(getInsertSQL("lina", "laguna blade", R.drawable.laguna_blade, 70, 60, 50));
		db.execSQL(getInsertSQL("lion", "finger of death", R.drawable.finger_of_death, 160, 100, 40));
		db.execSQL(getInsertSQL("luna", "eclipse", R.drawable.eclipse, 160, 150, 140));
		db.execSQL(getInsertSQL("lycan", "shapeshift", R.drawable.shapeshift, 120, 90, 60));
		db.execSQL(getInsertSQL("magnus", "reverse polarity", R.drawable.reverse_polarity, 120, 110, 100));
		db.execSQL(getInsertSQL("medusa", "stone gaze", R.drawable.stone_gaze, 90, 90, 90));
		db.execSQL(getInsertSQL("mirana", "moonlight shadow", R.drawable.moonlight_shadow, 140, 120, 100));
		db.execSQL(getInsertSQL("naga_siren", "song of the siren", R.drawable.song_of_the_siren, 180, 120, 60));
		db.execSQL(getInsertSQL("natures_prophet", "wrath of nature", R.drawable.wrath_of_nature, 90, 75, 60));
		db.execSQL(getInsertSQL("necrophos", "reapers scythe", R.drawable.reapers_scythe, 100, 85, 70));
		db.execSQL(getInsertSQL("omniknight", "guardian angel", R.drawable.guardian_angel, 150, 150, 150));
		db.execSQL(getInsertSQL("outworld devourer", "sanitys eclipse", R.drawable.sanitys_eclipse, 160, 160, 160));
		db.execSQL(getInsertSQL("phoenix", "supernova", R.drawable.supernova, 110, 110, 110));
		db.execSQL(getInsertSQL("puck", "dream coil", R.drawable.dream_coil, 85, 85, 85));
		db.execSQL(getInsertSQL("queen of pain", "sonic wave", R.drawable.sonic_wave, 135, 135, 40));
		db.execSQL(getInsertSQL("razor", "eye of the storm", R.drawable.eye_of_the_storm, 80, 70, 60));
		db.execSQL(getInsertSQL("sand king", "epicenter", R.drawable.epicenter, 140, 120, 100));
		db.execSQL(getInsertSQL("shadow shaman", "mass serpent ward", R.drawable.mass_serpent_ward, 120, 120, 120));
		db.execSQL(getInsertSQL("silencer", "global silence", R.drawable.global_silence, 130, 130, 130));
		db.execSQL(getInsertSQL("skywrath mage", "mystic flare", R.drawable.mystic_flare, 60, 40, 20));
		db.execSQL(getInsertSQL("slark", "shadow dance", R.drawable.shadow_dance, 60, 60, 60));
		db.execSQL(getInsertSQL("spectre", "haunt", R.drawable.haunt, 120, 120, 120));
		db.execSQL(getInsertSQL("sven", "gods strength", R.drawable.gods_strength, 80, 80, 80));
		db.execSQL(getInsertSQL("terrorblade", "metamorphosis", R.drawable.metamorphosis, 140, 140, 140));
		db.execSQL(getInsertSQL("terrorblade", "sunder", R.drawable.sunder, 120, 80, 40));
		db.execSQL(getInsertSQL("tidehunter", "ravage", R.drawable.ravage, 150, 150, 150));
		db.execSQL(getInsertSQL("treant protector", "overgrowth", R.drawable.overgrowth, 70, 70, 70));
		db.execSQL(getInsertSQL("undying", "flesh golem", R.drawable.flesh_golem, 75, 75, 75));
		db.execSQL(getInsertSQL("undying", "tombstone", R.drawable.tombstone, 60, 60, 60));
		db.execSQL(getInsertSQL("vengeful spirit", "nether swap", R.drawable.nether_swap, 45, 45, 45));
		db.execSQL(getInsertSQL("viper", "viper strike", R.drawable.viper_strike, 80, 50, 30));
		db.execSQL(getInsertSQL("warlock", "chaotic offering", R.drawable.chaotic_offering, 165, 165, 165));
		db.execSQL(getInsertSQL("weaver", "time lapse", R.drawable.time_lapse, 60, 50, 40));
		db.execSQL(getInsertSQL("witch doctor", "death ward", R.drawable.death_ward, 80, 80, 80));
		db.execSQL(getInsertSQL("wraith king", "reincarnation", R.drawable.reincarnation, 260, 160, 60));
		db.execSQL(getInsertSQL("zeus", "thundergods wrath", R.drawable.thundergods_wrath, 90, 90, 90));
		db.execSQL(getInsertSQL("black king bar", "bkb", R.drawable.black_king_bar, 80, 75, 70));
		db.execSQL(getInsertSQL("Refresher Orb", "Refresher Orb", R.drawable.refresher_orb, 185, 185, 185));
	}

	private String getInsertSQL(String heroName, String spellName,
			int pictureId, int cooldown1, int cooldown2, int cooldown3) {
		return "INSERT INTO " + DBConstant.DB_TABLE_SPELLS + " (`"
				+ DBConstant.SPELL_HERO + "`, `" + DBConstant.SPELL_NAME
				+ "`, `" + DBConstant.SPELL_PICTURE + "`, `"
				+ DBConstant.SPELL_COOLDOWN_1 + "`, `"
				+ DBConstant.SPELL_COOLDOWN_2 + "`, `"
				+ DBConstant.SPELL_COOLDOWN_3 + "` ) values ('" + heroName
				+ "', '" + spellName + "', '" + pictureId + "', '"
				+ cooldown1 + "', '" + cooldown2 + "', '" + cooldown3 + "') ";
	}
}
