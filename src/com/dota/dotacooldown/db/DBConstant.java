package com.dota.dotacooldown.db;

public class DBConstant {

	public static final String DB_TABLE_SPELLS = "spells";
	public static final String DB_TABLE_COOLDOWN = "cooldown";
	
	public static final String TABLE_COOLDOWN_ID = "_id";
	public static final String TABLE_COOLDOWN_SPELL_ID = "spell_id";
	public static final String TABLE_COOLDOWN_ADD_TIME = "add_time";
	public static final String TABLE_COOLDOWN_COOLDOWN_LVL = "cooldown_level";
	
	public static final String SPELL_ID = "_id";
	public static final String SPELL_NAME = "name";
	public static final String SPELL_PICTURE = "picture";
	public static final String SPELL_COOLDOWN_1 = "cooldown_1";
	public static final String SPELL_COOLDOWN_2 = "cooldown_2";
	public static final String SPELL_COOLDOWN_3 = "cooldown_3";
	public static final String SPELL_HERO = "hero";
	
	

}
