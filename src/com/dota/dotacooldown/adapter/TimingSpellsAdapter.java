package com.dota.dotacooldown.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.dota.dotacooldown.R;
import com.dota.dotacooldown.db.DBOperations;
import com.dota.dotacooldown.model.Spell;

public class TimingSpellsAdapter extends BaseAdapter implements Filterable {

	Context context;

	public TimingSpellsAdapter(Context context) {
		spells = DBOperations.SpellsTable.getAllSpells(context);
		this.context = context;
	}

	ArrayList<Spell> spells;

	@Override
	public int getCount() {
		return spells.size();
	}

	@Override
	public Spell getItem(int pos) {
		return spells.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(final int pos, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.item_spell, null);
		}

		ImageView picture = (ImageView) view
				.findViewById(R.id.item_spell_picture);

		picture.setImageResource(spells.get(pos).getPictureId());

		return view;
	}

	@Override
	public Filter getFilter() {
		return new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults filterResults) {
				spells = (ArrayList<Spell>) filterResults.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				spells = DBOperations.SpellsTable.getAllSpells(context);
				List<Spell> filterResults = new ArrayList<Spell>();
				FilterResults results = new FilterResults();
				if (constraint != null && constraint.toString().length() > 0) {
					filterResults = FilterSearch(constraint, spells);
					results.values = filterResults;
					results.count = filterResults.size();
				} else {
					results.values = spells;
					results.count = spells.size();
				}
				return results;
			}
		};
	}

	private List<Spell> FilterSearch(CharSequence charSequence,
			List<Spell> rawData) {
		List<Spell> result = new ArrayList<Spell>();
		for (Spell spell : rawData) {
			if (spell
					.getHeroName()
					.toUpperCase(Locale.ENGLISH)
					.contains(
							charSequence.toString().toUpperCase(Locale.ENGLISH))
					|| spell.getName()
							.toUpperCase(Locale.ENGLISH)
							.contains(
									charSequence.toString().toUpperCase(
											Locale.ENGLISH))) {
				result.add(spell);
			}
		}
		return result;
	}

}
