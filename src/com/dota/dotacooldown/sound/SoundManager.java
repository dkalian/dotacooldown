package com.dota.dotacooldown.sound;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;

public class SoundManager {

	/**
	 * @param context
	 * @param sound
	 */
	public static void playKeyClickSound(Context context, Sound sound) {
		SharedPreferences defaultPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		if (defaultPreferences.getBoolean("settings_key_click_sound", true)) {
			MediaPlayer mediaPlayer = MediaPlayer.create(context,
					sound.getSoundId());
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.start();
		}
	}

	public static void playNotificationSound(Context context, Sound sound) {
		MediaPlayer mediaPlayer = MediaPlayer.create(context,
				sound.getSoundId());
		mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mediaPlayer.start();
	}
}
