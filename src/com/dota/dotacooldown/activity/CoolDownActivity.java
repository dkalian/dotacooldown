package com.dota.dotacooldown.activity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.dota.dotacooldown.R;
import com.dota.dotacooldown.adapter.CooldownSpellsAdapter;
import com.dota.dotacooldown.db.DBOperations;
import com.dota.dotacooldown.model.Constant;
import com.dota.dotacooldown.model.SPManager;
import com.dota.dotacooldown.sound.Sound;
import com.dota.dotacooldown.sound.SoundManager;

public class CoolDownActivity extends ActionBarActivity {

	Button startRoshanButton;
	Button startNeutralsButton;
	View deleteAllButton;
	View deleteButton;
	ImageButton pauseButton;
	TextView roshanTimerTextView;
	TextView neutralsTimerTextView;
	View noSpellsMessageView;
	View pauseCover;
	CooldownSpellsAdapter cooldownSpellsAdapter;
	GridView cooldownSpellsGrid;
	boolean isTimersOnPause = false;
	long pauseTimeInMillis = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cooldown);
		sp = getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
		PreferenceManager.setDefaultValues(this, R.xml.settings, false);
		synchronizeTimer = new Timer();
		synchronizeTimer.schedule(synchronizeTask, 0, 250);
		startRoshanButton = (Button) findViewById(R.id.activity_cooldown_button_start_roshan);
		startNeutralsButton = (Button) findViewById(R.id.activity_cooldown_button_start_neutrals);
		deleteAllButton = findViewById(R.id.activity_cooldown_button_delete_all);
		deleteButton = findViewById(R.id.activity_cooldown_button_delete);
		roshanTimerTextView = (TextView) findViewById(R.id.activity_cooldown_roshan_timer);
		neutralsTimerTextView = (TextView) findViewById(R.id.activity_cooldown_neutrals_timer);
		pauseButton = (ImageButton) findViewById(R.id.activity_cooldown_button_pause);
		pauseCover = findViewById(R.id.activity_cooldown_pause_cover);
		noSpellsMessageView = findViewById(R.id.activity_cooldown_no_spells_message);
		startRoshanButton.setOnClickListener(startRoshanButtonListener);
		startNeutralsButton.setOnClickListener(startRoshanButtonListener);
		deleteAllButton.setOnClickListener(deleteButtonsClickListener);
		deleteButton.setOnClickListener(deleteButtonsClickListener);
		pauseButton.setOnClickListener(pauseButtonClickListener);
		cooldownSpellsGrid = (GridView) findViewById(R.id.activity_cooldown_spells_spells_grid);
		cooldownSpellsAdapter = new CooldownSpellsAdapter(this);
		initSpellsGrid();
	}

	OnClickListener pauseButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			SoundManager.playKeyClickSound(CoolDownActivity.this,
					Sound.PAUSE_CLICK);
			isTimersOnPause = !isTimersOnPause;
			if (isTimersOnPause) {
				pauseTimers();
			} else {
				unpauseTimers();
			}
		}
	};

	private void pauseTimers() {
		pauseCover.setVisibility(View.VISIBLE);
	}

	private void unpauseTimers() {
		pauseCover.setVisibility(View.INVISIBLE);
		DBOperations.Cooldown.updateTimersStartTime(this, pauseTimeInMillis);
		if (roshanTimerStartTime != 0)
			SPManager.putLong(sp, Constant.SP_KEY_ROSHAN_TIMER_START_TIME,
					roshanTimerStartTime + pauseTimeInMillis);
		if (neutralsTimerStartTime != 0)
			SPManager.putLong(sp, Constant.SP_KEY_NEUTRAL_TIMER_START_TIME,
					neutralsTimerStartTime + pauseTimeInMillis);
		pauseTimeInMillis = 0;
		cooldownSpellsAdapter.updateDataSet();
	}

	OnClickListener deleteButtonsClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			SoundManager.playKeyClickSound(CoolDownActivity.this,
					Sound.DELETE_CLICK);
			switch (v.getId()) {
			case R.id.activity_cooldown_button_delete_all:
				DBOperations.Cooldown.deleteAllSpells(CoolDownActivity.this);
				cooldownSpellsAdapter.updateDataSet();
				cooldownSpellsAdapter.notifyDataSetChanged();
				updateNoSpellsMessage();
				break;
			case R.id.activity_cooldown_button_delete:
				actionMode = startSupportActionMode(mActionModeCallback);
				break;
			}
		}
	};

	OnItemClickListener cooldownGridItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int pos,
				long arg3) {
			SoundManager.playKeyClickSound(CoolDownActivity.this,
					Sound.SPELL_CLICK);
			if (actionMode == null) {
				DBOperations.Cooldown
						.updateSpellTimerStartTime(CoolDownActivity.this,
								cooldownSpellsAdapter.getItem(pos)
										.getCooldownTableId());
				cooldownSpellsAdapter.updateDataSet();
				if (cooldownSpellsGrid.isItemChecked(pos)) {
					cooldownSpellsGrid.setItemChecked(pos, false);
				} else {
					cooldownSpellsGrid.setItemChecked(pos, true);
				}
			} else {
				if (cooldownSpellsGrid.getCheckedItemCount() <= 0) {
					actionMode.finish();
				}
			}
		}
	};

	OnItemLongClickListener cooldownGridItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View view, int pos,
				long arg3) {
			DBOperations.Cooldown.levelUp(CoolDownActivity.this,
					cooldownSpellsAdapter.getItem(pos));
			cooldownSpellsAdapter.updateDataSet();
			cooldownSpellsAdapter.notifyDataSetChanged();
			return true;
		}
	};

	ActionMode actionMode;
	ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.activity_cooldown_action_menu, menu);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode arg0, MenuItem menuItem) {
			SparseBooleanArray checkedItems = cooldownSpellsGrid
					.getCheckedItemPositions();
			switch (menuItem.getItemId()) {
			case R.id.action_delete:
				int[] idsForDelete = new int[checkedItems.size()];
				for (int i = 0; i < checkedItems.size(); i++) {
					idsForDelete[i] = cooldownSpellsAdapter.getItem(
							checkedItems.keyAt(i)).getCooldownTableId();
				}
				DBOperations.Cooldown.deleteCheckedSpells(
						CoolDownActivity.this, idsForDelete);
				cooldownSpellsAdapter.updateDataSet();
				cooldownSpellsAdapter.notifyDataSetChanged();
				actionMode.finish();
				updateNoSpellsMessage();
				break;
			}
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode arg0) {
			actionMode = null;
			cooldownSpellsGrid.clearChoices();
			cooldownSpellsGrid.requestLayout();
		}

		@Override
		public boolean onPrepareActionMode(ActionMode arg0, Menu arg1) {
			return false;
		}
	};

	private void initSpellsGrid() {
		updateNoSpellsMessage();
		cooldownSpellsGrid.setAdapter(cooldownSpellsAdapter);
		cooldownSpellsGrid
				.setOnItemClickListener(cooldownGridItemClickListener);
		cooldownSpellsGrid
				.setOnItemLongClickListener(cooldownGridItemLongClickListener);
		cooldownSpellsGrid.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
	}

	private void updateNoSpellsMessage() {
		if (cooldownSpellsAdapter != null)
			if (cooldownSpellsAdapter.getCount() == 0) {
				noSpellsMessageView.setVisibility(View.VISIBLE);
			} else {
				noSpellsMessageView.setVisibility(View.INVISIBLE);
			}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_cooldown_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add_cooldown_spell:
			startActivity(new Intent(this, AddSpellActivity.class));
			break;
		case R.id.action_settings:
			startActivity(new Intent(this, SettingsActivity.class));
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		synchronizeTimer.cancel();
	};

	@Override
	protected void onResume() {
		if (cooldownSpellsAdapter != null) {
			cooldownSpellsAdapter.updateDataSet();
			cooldownSpellsAdapter.notifyDataSetChanged();
		}
		updateNoSpellsMessage();
		super.onResume();
	};

	Timer synchronizeTimer;
	SharedPreferences sp;
	long roshanTimerStartTime = 0;
	long neutralsTimerStartTime = 0;

	TimerTask synchronizeTask = new TimerTask() {
		@Override
		public void run() {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					roshanTimerStartTime = sp.getLong(
							Constant.SP_KEY_ROSHAN_TIMER_START_TIME, 0);
					neutralsTimerStartTime = sp.getLong(
							Constant.SP_KEY_NEUTRAL_TIMER_START_TIME, 0);
					SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss",
							Locale.US);
					if (!isTimersOnPause) {
						if (roshanTimerStartTime != 0) {
							roshanTimerTextView.setText(dateFormat
									.format(new Date(System.currentTimeMillis()
											- roshanTimerStartTime)));
							startRoshanButton.setText("Clear");
							checkRoshanNotification(System.currentTimeMillis()
									- roshanTimerStartTime);
						} else {
							roshanTimerTextView.setText("00:00");
							startRoshanButton.setText("Start");
						}
						if (neutralsTimerStartTime != 0) {
							neutralsTimerTextView.setText(dateFormat
									.format(new Date(System.currentTimeMillis()
											- neutralsTimerStartTime)));
							startNeutralsButton.setText("Clear");
							checkNeutralsNotification(new Date(
									System.currentTimeMillis()
											- neutralsTimerStartTime));
						} else {
							neutralsTimerTextView.setText("00:00");
							startNeutralsButton.setText("Start");
						}
						if (cooldownSpellsAdapter != null) {
							cooldownSpellsAdapter.notifyDataSetChanged();
						}
					} else {
						pauseTimeInMillis += 250;
					}
				}
			});
		}
	};

	private void checkRoshanNotification(long currentTimerTime) {
		for (int i = 0; i < Constant.ROSHAN_NOTIFICATIONS_TIME_IN_MILLIS.length; i++) {
			if (currentTimerTime >= Constant.ROSHAN_NOTIFICATIONS_TIME_IN_MILLIS[i] - 250
					&& currentTimerTime <= Constant.ROSHAN_NOTIFICATIONS_TIME_IN_MILLIS[i]) {
				System.out.println("Time: " + currentTimerTime);
				SoundManager.playNotificationSound(CoolDownActivity.this,
						Sound.ROSHAN_TIMER_NOTIFICATION);
			}
		}
	}

	private void checkNeutralsNotification(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		long millis = calendar.get(Calendar.MILLISECOND);

		int seconds = date.getSeconds();
		if (seconds == 45 && (millis >= 0 && millis < 250)) {
			SoundManager.playNotificationSound(CoolDownActivity.this,
					Sound.ROSHAN_TIMER_NOTIFICATION);
		}
	}

	OnClickListener startRoshanButtonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			SoundManager.playKeyClickSound(CoolDownActivity.this,
					Sound.TIMER_CLICK);
			switch (v.getId()) {
			case R.id.activity_cooldown_button_start_roshan:
				if (sp.getLong(Constant.SP_KEY_ROSHAN_TIMER_START_TIME, 0) != 0) {
					SPManager.putLong(sp,
							Constant.SP_KEY_ROSHAN_TIMER_START_TIME, 0);
				} else {
					SPManager.putLong(sp,
							Constant.SP_KEY_ROSHAN_TIMER_START_TIME,
							System.currentTimeMillis());
				}
				break;
			case R.id.activity_cooldown_button_start_neutrals:
				if (sp.getLong(Constant.SP_KEY_NEUTRAL_TIMER_START_TIME, 0) != 0) {
					SPManager.putLong(sp,
							Constant.SP_KEY_NEUTRAL_TIMER_START_TIME, 0);
				} else {
					SPManager.putLong(sp,
							Constant.SP_KEY_NEUTRAL_TIMER_START_TIME,
							System.currentTimeMillis());
				}
				break;
			}
		}
	};
}
