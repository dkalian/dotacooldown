package com.dota.dotacooldown.model;

public class Spell {

	private int id;
	private int cooldown1;
	private int cooldown2;
	private int cooldown3;
	private String name;
	private String heroName;
	private int pictureId;
	
	// ONLY IN COOLDOWN TABLE!!!!!!!!
	private long timerStartTime;
	private int cooldownTableId;
	private int cooldownLevel = 1;
	
	
	public Spell() {
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the cooldown1
	 */
	public int getCooldown1() {
		return cooldown1;
	}

	/**
	 * @param cooldown1
	 *            the cooldown1 to set
	 */
	public void setCooldown1(int cooldown1) {
		this.cooldown1 = cooldown1;
	}

	/**
	 * @return the cooldown2
	 */
	public int getCooldown2() {
		return cooldown2;
	}

	/**
	 * @param cooldown2
	 *            the cooldown2 to set
	 */
	public void setCooldown2(int cooldown2) {
		this.cooldown2 = cooldown2;
	}

	/**
	 * @return the cooldown3
	 */
	public int getCooldown3() {
		return cooldown3;
	}

	/**
	 * @param cooldown3
	 *            the cooldown3 to set
	 */
	public void setCooldown3(int cooldown3) {
		this.cooldown3 = cooldown3;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the heroName
	 */
	public String getHeroName() {
		return heroName;
	}

	/**
	 * @param heroName
	 *            the heroName to set
	 */
	public void setHeroName(String heroName) {
		this.heroName = heroName;
	}

	/**
	 * @return the pictureId
	 */
	public int getPictureId() {
		return pictureId;
	}

	/**
	 * @param pictureId
	 *            the pictureId to set
	 */
	public void setPictureId(int pictureId) {
		this.pictureId = pictureId;
	}

	public long getTimerStartTime() {
		return timerStartTime;
	}

	public void setTimerStartTime(long timerStartTime) {
		this.timerStartTime = timerStartTime;
	}

	public int getCooldownTableId() {
		return cooldownTableId;
	}

	public void setCooldownTableId(int cooldownTableId) {
		this.cooldownTableId = cooldownTableId;
	}

	public int getCooldownLevel() {
		return cooldownLevel;
	}

	public void setCooldownLevel(int cooldownLevel) {
		this.cooldownLevel = cooldownLevel;
	}

}
