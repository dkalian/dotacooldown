package com.dota.dotacooldown.sound;

import com.dota.dotacooldown.R;

public enum Sound {

	SPELL_CLICK(R.raw.spell), DELETE_CLICK(R.raw.delete), TIMER_CLICK(
			R.raw.timer), PAUSE_CLICK(R.raw.pause), ROSHAN_TIMER_NOTIFICATION(
			R.raw.roshan_notification);

	private final int soundId;

	private Sound(int soundId) {
		this.soundId = soundId;
	}

	public int getSoundId() {
		return soundId;
	}
}
