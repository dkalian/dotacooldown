package com.dota.dotacooldown.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

import com.dota.dotacooldown.model.Spell;

public class DBOperations {

	public static class SpellsTable {

		public static ArrayList<Spell> getAllSpells(Context context) {
			ArrayList<Spell> spellsList = new ArrayList<Spell>();
			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			Cursor c = db.query(DBConstant.DB_TABLE_SPELLS, null, null, null,
					null, null, null);
			if (c.moveToFirst()) {
				do {
					int idf = c.getColumnIndex(DBConstant.SPELL_ID);
					int spellNamef = c.getColumnIndex(DBConstant.SPELL_NAME);
					int heroNamef = c.getColumnIndex(DBConstant.SPELL_HERO);
					int picturef = c.getColumnIndex(DBConstant.SPELL_PICTURE);
					int cooldown1f = c
							.getColumnIndex(DBConstant.SPELL_COOLDOWN_1);
					int cooldown2f = c
							.getColumnIndex(DBConstant.SPELL_COOLDOWN_2);
					int cooldown3f = c
							.getColumnIndex(DBConstant.SPELL_COOLDOWN_3);

					String id = c.getString(idf);
					String spellName = c.getString(spellNamef);
					String heroName = c.getString(heroNamef);
					int picture = c.getInt(picturef);
					String cooldown1 = c.getString(cooldown1f);
					String cooldown2 = c.getString(cooldown2f);
					String cooldown3 = c.getString(cooldown3f);

					Spell spell = new Spell();

					spell.setId(Integer.parseInt(id));
					spell.setHeroName(heroName);
					spell.setName(spellName);
					spell.setPictureId(picture);
					spell.setCooldown1(Integer.parseInt(cooldown1));
					spell.setCooldown2(Integer.parseInt(cooldown2));
					spell.setCooldown3(Integer.parseInt(cooldown3));

					spellsList.add(spell);
				} while (c.moveToNext());
			}
			db.close();
			return spellsList;
		}

		public static Spell getSpellById(Context context, int id) {
			Spell spell = new Spell();
			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			Cursor c = db.query(DBConstant.DB_TABLE_SPELLS, null,
					DBConstant.SPELL_ID + " = " + id, null, null, null, null);
			if (c.moveToFirst()) {
				int idf = c.getColumnIndex(DBConstant.SPELL_ID);
				int spellNamef = c.getColumnIndex(DBConstant.SPELL_NAME);
				int heroNamef = c.getColumnIndex(DBConstant.SPELL_HERO);
				int picturef = c.getColumnIndex(DBConstant.SPELL_PICTURE);
				int cooldown1f = c.getColumnIndex(DBConstant.SPELL_COOLDOWN_1);
				int cooldown2f = c.getColumnIndex(DBConstant.SPELL_COOLDOWN_2);
				int cooldown3f = c.getColumnIndex(DBConstant.SPELL_COOLDOWN_3);

				String sid = c.getString(idf);
				String spellName = c.getString(spellNamef);
				String heroName = c.getString(heroNamef);
				String picture = c.getString(picturef);
				String cooldown1 = c.getString(cooldown1f);
				String cooldown2 = c.getString(cooldown2f);
				String cooldown3 = c.getString(cooldown3f);

				spell.setId(Integer.parseInt(sid));
				spell.setHeroName(heroName);
				spell.setName(spellName);
				spell.setPictureId(Integer.parseInt(picture));
				spell.setCooldown1(Integer.parseInt(cooldown1));
				spell.setCooldown2(Integer.parseInt(cooldown2));
				spell.setCooldown3(Integer.parseInt(cooldown3));
			}
			db.close();
			return spell;
		}
	}

	public static class Cooldown {

		public static void addSpell(Context context, int spellId) {
			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(DBConstant.TABLE_COOLDOWN_SPELL_ID, spellId);
			cv.put(DBConstant.TABLE_COOLDOWN_ADD_TIME, 0);
			cv.put(DBConstant.TABLE_COOLDOWN_COOLDOWN_LVL, 1);
			db.insert(DBConstant.DB_TABLE_COOLDOWN, null, cv);
			db.close();
		}

		public static ArrayList<Spell> getAllSpells(Context context) {
			ArrayList<Spell> spellsList = new ArrayList<Spell>();
			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			Cursor c = db.query(DBConstant.DB_TABLE_COOLDOWN, null, null, null,
					null, null, null);
			if (c.moveToFirst()) {
				do {
					int idf = c
							.getColumnIndex(DBConstant.TABLE_COOLDOWN_SPELL_ID);
					int cooldownTableIdF = c
							.getColumnIndex(DBConstant.TABLE_COOLDOWN_ID);
					int timerStartTimef = c
							.getColumnIndex(DBConstant.TABLE_COOLDOWN_ADD_TIME);
					int levelf = c
							.getColumnIndex(DBConstant.TABLE_COOLDOWN_COOLDOWN_LVL);

					Spell spell = DBOperations.SpellsTable.getSpellById(
							context, c.getInt(idf));
					spell.setTimerStartTime(c.getLong(timerStartTimef));
					spell.setCooldownTableId(c.getInt(cooldownTableIdF));
					spell.setCooldownLevel(c.getInt(levelf));
					spellsList.add(spell);
				} while (c.moveToNext());
			}
			db.close();
			return spellsList;
		}

		public static void updateSpellTimerStartTime(Context context,
				int cooldownTableId) {
			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(DBConstant.TABLE_COOLDOWN_ADD_TIME,
					System.currentTimeMillis());
			db.update(DBConstant.DB_TABLE_COOLDOWN, cv,
					DBConstant.TABLE_COOLDOWN_ID + " = " + cooldownTableId,
					null);
			db.close();
		}
		
		public static void updateSpellTimerStartTime(Context context,
				int cooldownTableId, long newAddTime) {
			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(DBConstant.TABLE_COOLDOWN_ADD_TIME, newAddTime);
			db.update(DBConstant.DB_TABLE_COOLDOWN, cv, DBConstant.TABLE_COOLDOWN_ID + " = " + cooldownTableId, null);
			db.close();
		}

		public static void deleteCheckedSpells(Context context, int[] idArray) {
			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			for (int i = 0; i < idArray.length; i++) {
				db.delete(DBConstant.DB_TABLE_COOLDOWN, "_id = " + idArray[i],
						null);
			}
			db.close();
		}

		public static void levelUp(Context context, Spell spell) {
			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			int newLevel = spell.getCooldownLevel() + 1;
			System.out.println("new lvl: " + newLevel);
			System.out.println("cooldown ID: " + spell.getCooldownTableId());
			if (newLevel <= 3
					|| (newLevel <= 6 && spell.getName().equals("bkb"))) {
				ContentValues cv = new ContentValues();
				cv.put(DBConstant.TABLE_COOLDOWN_ADD_TIME, 0);
				cv.put(DBConstant.TABLE_COOLDOWN_COOLDOWN_LVL, newLevel);
				db.update(DBConstant.DB_TABLE_COOLDOWN, cv, DBConstant.TABLE_COOLDOWN_ID + " = " 
						+ spell.getCooldownTableId(), null);
			}
			db.close();
		}

		public static void updateTimersStartTime(Context context,
				long timePlusInMillis) {
			ArrayList<Spell> cooldownSpells = getAllSpells(context);
			for (Spell spell : cooldownSpells) {
				long newTime = spell.getTimerStartTime() + timePlusInMillis;
				updateSpellTimerStartTime(context, spell.getCooldownTableId(), newTime);
			}
		}

		public static void deleteAllSpells(Context context) {
			DAO dao = new DAO(context);
			SQLiteDatabase db = dao.getWritableDatabase();
			db.delete(DBConstant.DB_TABLE_COOLDOWN, null, null);
			db.close();
		}
	}
}
