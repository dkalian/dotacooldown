package com.dota.dotacooldown.model;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SPManager {
	public static void putLong(SharedPreferences sp, String key, long value) {
		Editor editor = sp.edit();
		editor.putLong(key, value);
		editor.commit();
	}
}
