package com.dota.dotacooldown.model;

public class Constant {
	public static final String SP_NAME = "dota_cooldown_sp";

	public static final String SP_KEY_ROSHAN_TIMER_START_TIME = "roshan_timer_start_time";
	public static final String SP_KEY_NEUTRAL_TIMER_START_TIME = "neutrals_timer_start_time";

	public static final long[] ROSHAN_NOTIFICATIONS_TIME_IN_MILLIS = { 270000,
			480000, 660000 }; // 4:30, 8, 11

	public static class Settings {
		public static final String KEY_NOTICE = "settings_key_notice";
		public static final String KEY_DELAY = "settings_key_delay_time";

		/*
		 * NOTICE SETTING CODES
		 */
		public static final int NOTICE_OFF = 3;
		public static final int NOTICE_SOUND = 2;
		public static final int NOTICE_VOICE = 1;
	}
}
